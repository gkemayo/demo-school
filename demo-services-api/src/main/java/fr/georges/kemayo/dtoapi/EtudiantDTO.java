/**
 * 
 */
package fr.georges.kemayo.dtoapi;

import java.io.Serializable;

/**
 * @author Dell
 *
 */
public class EtudiantDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8255018496864997659L;
	
	/**
	 * 
	 */
	public EtudiantDTO() {
		//constructeur vide
	}
	
	/**
	 * @param pAge
	 * @param pNom
	 * @param pPrenom
	 */
	public EtudiantDTO(Integer pAge, String pNom, String pPrenom) {
		super();
		age = pAge;
		nom = pNom;
		prenom = pPrenom;
	}



	private Integer matricule;

	private Integer age;

	private String nom;

	private String prenom;

	public Integer getMatricule() {
		return matricule;
	}

	public void setMatricule(Integer pMatricule) {
		matricule = pMatricule;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer pAge) {
		age = pAge;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String pNom) {
		nom = pNom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String pPrenom) {
		prenom = pPrenom;
	}

}
