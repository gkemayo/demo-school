/**
 * 
 */
package fr.georges.kemayo.imanagerpresentation;

import fr.georges.kemayo.dtoapi.EtudiantDTO;

/**
 * @author Dell
 *
 */
public interface EtudiantManagerPresentation {
	
	public EtudiantDTO creerEtudiant(EtudiantDTO pEtudiantDTO);

}
