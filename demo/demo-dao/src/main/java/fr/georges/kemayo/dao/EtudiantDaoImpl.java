/**
 * 
 */
package fr.georges.kemayo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import fr.georges.kemayo.idao.EtudiantDao;
import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
@Repository("etudiantDao")
public class EtudiantDaoImpl extends DaoCommun implements EtudiantDao {

	private static final Logger LOG = LogManager.getLogger(EtudiantDaoImpl.class);

	public Integer saveEtudiant(Etudiant etu) {
		try {
			getCurrentSession().saveOrUpdate(etu);
		} catch (Exception e) {
			LOG.error("Erreur lors de la sauvegarde de l'objet", e);
		}
		return etu.getMatricule();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.georges.kemayo.idao.EtudiantDao#getAllEtudiants()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Etudiant> getAllEtudiants() {
		return getCurrentSession().createQuery("FROM Etudiant").list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.georges.kemayo.idao.EtudiantDao#getOneEtudiants(java.lang.Integer)
	 */
	@Override
	public Etudiant getEtudiantByMatricule(Integer pMatriculeEtu) {
		return (Etudiant) getCurrentSession().createQuery("FROM Etudiant etu WHERE etu.matricule = :mat")
				.setParameter("mat", pMatriculeEtu).uniqueResult();
	}

	/* (non-Javadoc)
	 * @see fr.georges.kemayo.idao.EtudiantDao#deleteEtudiantByMatricule(java.lang.Integer)
	 */
	@Override
	public Integer deleteEtudiantByNom(String pNomEtu) {
		return getCurrentSession().createQuery("DELETE FROM Etudiant etu WHERE etu.nom = :nom")
				.setParameter("nom", pNomEtu).executeUpdate();
	}
	
	

}
