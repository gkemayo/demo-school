/**
 * 
 */
package fr.georges.kemayo.idao;

import java.util.List;

import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
public interface EtudiantDao {

	public Integer saveEtudiant(Etudiant etu);
	
	public List<Etudiant> getAllEtudiants();
	
	public Etudiant getEtudiantByMatricule(Integer matriculeEtu);
	
	public Integer deleteEtudiantByNom(String pNomEtu);

}
