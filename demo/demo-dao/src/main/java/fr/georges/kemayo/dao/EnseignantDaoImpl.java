/**
 * 
 */
package fr.georges.kemayo.dao;

import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import fr.georges.kemayo.idao.EnseignantDao;
import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
@Repository("enseignantDao")
public class EnseignantDaoImpl extends DaoCommun implements EnseignantDao {
	
	private static final Logger LOG = LogManager.getLogger(EnseignantDaoImpl.class);
	
	public Integer saveEnseignant(Enseignant ens) {
		try {
			getCurrentSession().saveOrUpdate(ens);
		} catch (Exception e) {
			LOG.error("Erreur lors de la sauvegarde de l'objet", e);
		}
		return ens.getIdEns();
	}


}
