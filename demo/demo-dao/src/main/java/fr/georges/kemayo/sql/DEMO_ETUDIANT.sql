CREATE TABLE DEMO_ETUDIANT (
	MATRICULE NUMBER(12) NOT NULL,
	NOM VARCHAR(60), 
	PRENOM VARCHAR(60), 
	AGE NUMBER(6)
);

ALTER TABLE DEMO_ETUDIANT ADD CONSTRAINT DEMO_ETUDIANT_PK PRIMARY KEY (MATRICULE);

COMMENT ON COLUMN DEMO_ETUDIANT.MATRICULE IS 'MATRICULE ETUDIANT';
COMMENT ON COLUMN DEMO_ETUDIANT.NOM IS 'NOM ETUDIANT';
COMMENT ON COLUMN DEMO_ETUDIANT.PRENOM IS 'PRENOM ETUDIANT';
COMMENT ON COLUMN DEMO_ETUDIANT.AGE IS 'AGE ETUDIANT';