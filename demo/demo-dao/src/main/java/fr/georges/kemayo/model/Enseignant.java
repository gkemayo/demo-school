/**
 * 
 */
package fr.georges.kemayo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.georges.kemayo.util.DaoConstant;

/**
 * @author Dell
 *
 */
@Entity
@Table(name = "DEMO_ENSEIGNANT")
public class Enseignant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7689754577221596627L;
	
	
	/**
	 * 
	 */
	public Enseignant() {
		//constructeur vide
	}

	/**
	 * @param pIdEns
	 * @param pNom
	 * @param pPrenom
	 * @param pAge
	 */
	public Enseignant(String pNom, String pPrenom, Integer pAge) {
		nom = pNom;
		prenom = pPrenom;
		age = pAge;
	}

	private Integer idEns;

	private String nom;

	private String prenom;

	private Integer age;


	/**
	 * @return the idEns
	 */
	@Id
	@SequenceGenerator(name ="SEQ_DEMO_ENSEIGNANT", sequenceName = "SEQ_DEMO_ENSEIGNANT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEMO_ENSEIGNANT")
	public Integer getIdEns() {
		return idEns;
	}

	/**
	 * @param pIdEns the idEns to set
	 */
	public void setIdEns(Integer pIdEns) {
		idEns = pIdEns;
	}

	/**
	 * @return the nom
	 */
	@Column(name = "NOM")
	public String getNom() {
		return nom;
	}

	/**
	 * @param pNom the nom to set
	 */
	public void setNom(String pNom) {
		nom = pNom;
	}

	/**
	 * @return the prenom
	 */
	@Column(name = "PRENOM")
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param pPrenom the prenom to set
	 */
	public void setPrenom(String pPrenom) {
		prenom = pPrenom;
	}

	/**
	 * @return the age
	 */
	@Column(name = "AGE")
	public Integer getAge() {
		return age;
	}

	/**
	 * @param pAge the age to set
	 */
	public void setAge(Integer pAge) {
		age = pAge;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getIdEns()).hashCode() + DaoConstant.HASHCODE_PRIME_NUMBER;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Enseignant)) {
			return false;
		}
		Enseignant other = (Enseignant) obj;

		EqualsBuilder builder = new EqualsBuilder();

		return builder.append(getIdEns(), other.idEns).isEquals();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Enseignant [idEns=" + idEns + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}
	
}
