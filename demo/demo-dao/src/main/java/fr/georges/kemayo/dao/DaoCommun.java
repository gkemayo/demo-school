/**
 * 
 */
package fr.georges.kemayo.dao;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Dell
 *
 */
// @Repository("daoCommun")
public abstract class DaoCommun { // extends HibOernateDaoSupport

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Récupération de la session courante hibernate
	 * @param clazz
	 * @param id
	 * @return
	 */
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/**
	 * Pour récupérer une entity ayant une clé de type Integer
	 * @param clazz
	 * @param id
	 * @return
	 */
	public <T> T get(Class<T> clazz, Integer id) {
		return getCurrentSession().get(clazz, id);
	}
	
	/**
	 * Pour récupérer une entity ayant une clé de type String
	 * @param clazz
	 * @param id
	 * @return
	 */
	public <T> T get(Class<T> clazz, String id) {
		return getCurrentSession().get(clazz, id);
	}
	
	/**
	 * Pour récupérer une entity ayant une clé composite
	 * @param clazz
	 * @param id
	 * @return
	 */
	public <T> T get(Class<T> clazz, Serializable id) {
		return getCurrentSession().get(clazz, id);
	}

}
