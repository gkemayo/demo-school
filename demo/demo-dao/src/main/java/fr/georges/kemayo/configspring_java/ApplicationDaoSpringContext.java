package fr.georges.kemayo.configspring_java;
/**
 * 
 */

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Dell
 *
 */
@Configuration
@ComponentScan(basePackages = "fr.georges.kemayo.dao")
public class ApplicationDaoSpringContext {

}
