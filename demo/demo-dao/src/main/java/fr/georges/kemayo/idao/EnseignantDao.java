/**
 * 
 */
package fr.georges.kemayo.idao;

import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
public interface EnseignantDao {
	
	/**
	 * Savegarde un enseignant en base
	 * 
	 * @param ens
	 * @return
	 */
	public Integer saveEnseignant(Enseignant ens);

}
