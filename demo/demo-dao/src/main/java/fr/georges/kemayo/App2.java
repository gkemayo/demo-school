package fr.georges.kemayo;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.georges.kemayo.idao.EtudiantDao;
import fr.georges.kemayo.model.Etudiant;

/**
 * Hello world!
 *
 */
public class App2 {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:HibernateConfig.xml");
		EtudiantDao etudiantDao = (EtudiantDao) ctx.getBean("etudiantDao");
		Etudiant etudiant = new Etudiant("Kemayo", "Georges", 30);
		etudiantDao.saveEtudiant(etudiant);
		
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ctx.getBean("hibernateTemplate");

		Etudiant etu = hibernateTemplate.get(Etudiant.class, etudiant.getMatricule());
		assert (etu == null) : "Etudiant null";

		ctx.close();
	}
}
