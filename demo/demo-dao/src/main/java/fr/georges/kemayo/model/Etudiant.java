/**
 * 
 */
package fr.georges.kemayo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.georges.kemayo.util.DaoConstant;

/**
 * @author Dell
 *
 */
@Entity
@Table(name = "DEMO_ETUDIANT")
public class Etudiant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5171064406557183259L;

	private Integer matricule;

	private String nom;

	private String prenom;

	private Integer age;

	/**
	 * Constructeur par defaut
	 */
	public Etudiant() {
		// necéssaire à hibernate
	}

	/**
	 * @param nom
	 * @param prenom
	 * @param age
	 */
	public Etudiant(String nom, String prenom, Integer age) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}

	@Id
	@SequenceGenerator(name = "SEQ_DEMO_ETUDIANT", sequenceName = "SEQ_DEMO_ETUDIANT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEMO_ETUDIANT")
	// @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "MATRICULE")
	public Integer getMatricule() {
		return matricule;
	}

	public void setMatricule(Integer matricule) {
		this.matricule = matricule;
	}

	@Column(name = "NOM", nullable = false)
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(name = "PRENOM")
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Column(name = "AGE")
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getMatricule()).hashCode() + DaoConstant.HASHCODE_PRIME_NUMBER;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Etudiant)) {
			return false;
		}
		Etudiant other = (Etudiant) obj;

		EqualsBuilder builder = new EqualsBuilder();

		return builder.append(getMatricule(), other.matricule).isEquals();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Etudiant [matricule=" + matricule + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}
	
}
