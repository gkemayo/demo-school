package fr.georges.kemayo.config;
/**
 * 
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.georges.kemayo.configspring_java.ApplicationDaoSpringContext;

/**
 * @author Dell
 *
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { HibernateTestConfig.class, ApplicationDaoSpringContext.class })
// @ContextConfiguration(locations = {"classpath:HibernateTestConfig.xml"}) //,
// "classpath:ApplicationDaoContext.xml"
@Rollback // (<=> @TransactionConfiguration(defaultRollback = true))
@Transactional
public abstract class TestWithBaseEmbarqueeConfigInitializer {

	@Autowired
	protected HibernateTemplate hibernateTemplate;

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSessionForTest() {
		return sessionFactory.getCurrentSession();
	}

}
