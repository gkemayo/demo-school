package fr.georges.kemayo.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.georges.kemayo.config.TestWithBaseEmbarqueeConfigInitializer;
import fr.georges.kemayo.idao.EtudiantDao;
import fr.georges.kemayo.model.Etudiant;

/**
 * Unit test for simple App.
 */
public class EtudiantDaoImplTest extends TestWithBaseEmbarqueeConfigInitializer {

	private Etudiant etudiant;

	@Autowired
	private EtudiantDao etudiantDao;
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Before
	public void setUp() {
		etudiant = new Etudiant("Kemayo", "Georges", 30);
	}

	@Test
	public void testSaveEtudiant() {
		etudiantDao.saveEtudiant(etudiant);

		Etudiant etuRep = getCurrentSessionForTest().get(Etudiant.class, etudiant.getMatricule());
		assertNotNull(etuRep);
		assertEquals(etudiant.getNom(), etuRep.getNom());

	}

}
