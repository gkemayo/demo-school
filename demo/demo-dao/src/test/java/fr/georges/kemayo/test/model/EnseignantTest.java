/**
 * 
 */
package fr.georges.kemayo.test.model;

import org.junit.Test;

import fr.georges.kemayo.model.Enseignant;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

/**
 * @author Dell
 *
 */
public class EnseignantTest {
	
	@Test
	public void testEqualAndHashCodeEtudiant() {
		EqualsVerifier.forClass(Enseignant.class).suppress(Warning.STRICT_INHERITANCE).withOnlyTheseFields("idEns")
				.verify();
	}

}
