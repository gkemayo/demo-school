package fr.georges.kemayo.test.model;

import org.junit.Test;

import fr.georges.kemayo.model.Etudiant;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class EtudiantTest {

	@Test
	public void testEqualAndHashCodeEtudiant() {
		EqualsVerifier.forClass(Etudiant.class).suppress(Warning.STRICT_INHERITANCE).withOnlyTheseFields("matricule")
				.verify();
	}

}
