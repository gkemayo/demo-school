package fr.georges.kemayo.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fr.georges.kemayo.config.TestWithBaseEmbarqueeConfigInitializer;
import fr.georges.kemayo.idao.EnseignantDao;
import fr.georges.kemayo.model.Enseignant;
import fr.georges.kemayo.model.Etudiant;

/**
 * Unit test for simple App.
 */
public class EnseignantDaoImplTest extends TestWithBaseEmbarqueeConfigInitializer {

	private Enseignant enseignant;

	@Autowired
	private EnseignantDao enseignantDao;
	
	@Before
	public void setUp() {
		enseignant = new Enseignant("Kemayo", "Georges", 30);
	}

	@Test
	public void testSaveEtudiant() {
		enseignantDao.saveEnseignant(enseignant);

		Enseignant ensRep = getCurrentSessionForTest().get(Enseignant.class, enseignant.getIdEns());
		assertNotNull(ensRep);
		assertEquals(enseignant.getNom(), ensRep.getNom());

	}

}
