/**
 * 
 */
package fr.georges.kemayo.test.managerpresentation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.annotation.DirtiesContext;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.dtoapi.EtudiantDTO;
import fr.georges.kemayo.imanager.EtudiantManager;
import fr.georges.kemayo.imanagerpresentation.EtudiantManagerPresentation;
import fr.georges.kemayo.managerpresentation.EtudiantManagerPresentationImpl;
import fr.georges.kemayo.mapper.EtudiantServiceMapper;

/**
 * @author Dell
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EtudiantManagerPresentationTest {
	
	@InjectMocks
	private EtudiantManagerPresentation etudiantManagerPresentation = new EtudiantManagerPresentationImpl();
	
	@Mock
	private EtudiantManager etudiantManager;
	
	private EtudiantDTO etudiantDTO;

	private EtudiantDto etudiantDto;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		etudiantDTO = new EtudiantDTO();
		etudiantDTO.setAge(30);
		etudiantDTO.setNom("Kemayo Test");
		etudiantDTO.setPrenom("Georges Test");
		
		etudiantDto = EtudiantServiceMapper.mapEtudiantDTOtoEtudiantDto(etudiantDTO);
		etudiantDto.setMatricule(1);
	}

	@Test
	@DirtiesContext
	public void testCreerEtudiant() {
		
		Mockito.when(etudiantManager.creerEtudiant(Mockito.any(EtudiantDto.class))).thenReturn(etudiantDto);

		EtudiantDTO etudiantDTOResult = etudiantManagerPresentation.creerEtudiant(etudiantDTO);
		
		Mockito.verify(etudiantManager).creerEtudiant(Mockito.any(EtudiantDto.class));

		assertNotNull(etudiantDTOResult);
		assertNotNull(etudiantDTOResult.getMatricule());
		assertEquals(etudiantDto.getAge(), etudiantDTOResult.getAge());
		assertEquals(etudiantDto.getNom(), etudiantDTOResult.getNom());
		assertEquals(etudiantDto.getPrenom(), etudiantDTOResult.getPrenom());

	}

}
