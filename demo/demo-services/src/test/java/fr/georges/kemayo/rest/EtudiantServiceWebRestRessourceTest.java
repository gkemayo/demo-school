/**
 * 
 */
package fr.georges.kemayo.rest;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import fr.georges.kemayo.config.TestIntegrationServiceWithBaseEmbarqueeConfigInitializer;
import fr.georges.kemayo.dtoapi.EtudiantDTO;
import fr.georges.kemayo.imanagerpresentation.EtudiantManagerPresentation;

/**
 * @author Dell
 *
 */
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class EtudiantServiceWebRestRessourceTest extends TestIntegrationServiceWithBaseEmbarqueeConfigInitializer {
	
	@Autowired
	private EtudiantManagerPresentation etudiantManagerPres;
	
	private EtudiantDTO etu1 = new EtudiantDTO(21, "Belloti", "Romain");
	
	private EtudiantDTO etu2 = new EtudiantDTO(22, "Dupont", "Marc");
	
	public void setUp(){
		etudiantManagerPres.creerEtudiant(etu1);
		etudiantManagerPres.creerEtudiant(etu2);
	}
	
	@Test
	public void testRestGetAllEtudiants() throws ClientProtocolException, IOException{
		
		 // Given
	    HttpUriRequest request = new HttpGet("http://localhost:9991/demo-services/wsrest/etudiant/fullrecherche");
	 
	    // When
	    HttpResponse response = HttpClientBuilder.create().build().execute(request);
		
	    assertNotNull(response);
	}

}
