/**
 * 
 */
package fr.georges.kemayo.test.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.dtoapi.EtudiantDTO;
import fr.georges.kemayo.mapper.EtudiantServiceMapper;

/**
 * @author Dell
 *
 */
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class EtudiantServiceMapperTest {
	
private EtudiantDto etudiantDto;
	
	@Before
	public void setUp(){
		etudiantDto = new EtudiantDto();
		etudiantDto.setAge(30);
		etudiantDto.setNom("Kemayo Test");
		etudiantDto.setPrenom("Georges Test");
	}
	
	@Test
	public void testGetEtudiantDTOFromEtudiantDto(){
		
		EtudiantDTO etudiantDTO = EtudiantServiceMapper.getEtudiantDTOFromEtudiantDto(etudiantDto);
		
		assertNotNull(etudiantDTO);
		assertEquals(etudiantDto.getAge(), etudiantDTO.getAge());
		assertEquals(etudiantDto.getNom(), etudiantDTO.getNom());
		assertEquals(etudiantDto.getPrenom(), etudiantDTO.getPrenom());
		
	}
	
	@Test
	public void testMapEtudiantDTOtoEtudiantDto(){
		
		EtudiantDTO etudiantDTO = EtudiantServiceMapper.getEtudiantDTOFromEtudiantDto(etudiantDto);
		
		EtudiantDto etudiantDto = EtudiantServiceMapper.mapEtudiantDTOtoEtudiantDto(etudiantDTO);
		
		assertNotNull(etudiantDTO);
		assertEquals(etudiantDTO.getAge(), etudiantDto.getAge());
		assertEquals(etudiantDTO.getNom(), etudiantDto.getNom());
		assertEquals(etudiantDTO.getPrenom(), etudiantDto.getPrenom());
		
	}

}
