/**
 * 
 */
package fr.georges.kemayo.wsrest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.imanager.EtudiantManager;
import fr.georges.kemayo.wsrest.mapper.EtudiantServiceRestMapper;
import fr.georges.kemayo.wsrest.representation.EtudiantRestDTO;
import fr.georges.kemayo.wsrest.representation.RestResponseMajDTO;

/**
 * @author Dell
 *
 */
@Path("/etudiant")
//@Produces(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_JSON)
public class EtudiantWebServiceRestRessource {
	
	@Autowired
	private EtudiantManager etudiantManager; //= (EtudiantManager) ApplicationContext.getBean("EtudiantManager");
	
	@PostConstruct
	public void init() {
	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}
	
	@POST
	@Path("/creation")
	public Response creerEtudiant(@QueryParam("age") Integer pAge, @QueryParam("nom") String pNom, @QueryParam("prenom") String pPrenom){
		EtudiantRestDTO etudiantRestDTO = new EtudiantRestDTO(pAge, pNom, pPrenom);
		EtudiantDto etudiantDto = EtudiantServiceRestMapper.getEtudiantDtoFromEtudiantRestDTO(etudiantRestDTO);
		etudiantDto = etudiantManager.creerEtudiant(etudiantDto);
		if(etudiantDto.getMatricule() == null){
			return Response.status(Status.NO_CONTENT).build();
		}
		RestResponseMajDTO restResponseMajDTO = new RestResponseMajDTO();
		restResponseMajDTO.setResultatOpertion("L'etudiant " + pNom + " a bien ete cree");
		GenericEntity<RestResponseMajDTO> entity = new GenericEntity<RestResponseMajDTO>(restResponseMajDTO){};
		return Response.status(Status.CREATED).entity(entity).build();
	}

	@GET
	@Path("/fullrecherche")
	public Response getAllEtudiants(){
		List<EtudiantDto> listeEtu = etudiantManager.getAllEtudiants();
		if(CollectionUtils.isEmpty(listeEtu)){
			return Response.status(Status.NO_CONTENT).build();
		}
		List<EtudiantRestDTO> listeEtuRest = new ArrayList<>();
		for (EtudiantDto etudiantDto : listeEtu) {
			listeEtuRest.add(EtudiantServiceRestMapper.mapEtudiantDtoToEtudiantRestDTO(etudiantDto));
		}
		GenericEntity<List<EtudiantRestDTO>> entity = new GenericEntity<List<EtudiantRestDTO>>(listeEtuRest){};
		return Response.status(Status.OK).entity(entity).build();
	}

	@GET
	@Path("/recherche/{mat}")
	public Response getEtudiantByMatricule(@PathParam("mat") Integer pMatriculeEtu){
		EtudiantDto etu = etudiantManager.getEtudiantByMatricule(pMatriculeEtu);
		if(etu == null){
			return Response.status(Status.NO_CONTENT).build();
		}
		EtudiantRestDTO etuRest = EtudiantServiceRestMapper.mapEtudiantDtoToEtudiantRestDTO(etu);;
		GenericEntity<EtudiantRestDTO> entity = new GenericEntity<EtudiantRestDTO>(etuRest){};
		return Response.status(Status.OK).entity(entity).build();
	}
	
	@DELETE
	@Path("/supprimer/{nom}")
	public Response deleteEtudiantByMatricule(@PathParam("nom") String pNomEtu){
		Integer matSupp = etudiantManager.deleteEtudiantByNom(pNomEtu);
		if(matSupp.equals(0)){
			return Response.status(Status.NOT_MODIFIED).build();
		}
		RestResponseMajDTO restResponseMajDTO = new RestResponseMajDTO();
		restResponseMajDTO.setResultatOpertion("L'etudiant " + pNomEtu + " a bien ete supprime");
		GenericEntity<RestResponseMajDTO> entity = new GenericEntity<RestResponseMajDTO>(restResponseMajDTO){};
		return Response.status(Status.OK).entity(entity).build();
	}

}
