/**
 * 
 */
package fr.georges.kemayo.wsrest.mapper;

import org.modelmapper.ModelMapper;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.wsrest.representation.EtudiantRestDTO;

/**
 * @author Dell
 *
 */
public class EtudiantServiceRestMapper {
	
	/**
	 * 
	 */
	private EtudiantServiceRestMapper() {
		//empty constructor
	}

	public static EtudiantRestDTO mapEtudiantDtoToEtudiantRestDTO(EtudiantDto etu) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(etu, EtudiantRestDTO.class);
	}
	
	public static EtudiantDto getEtudiantDtoFromEtudiantRestDTO(EtudiantRestDTO pEtudiantRestDTO) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(pEtudiantRestDTO, EtudiantDto.class);
	}

}
