/**
 * 
 */
package fr.georges.kemayo.wsrest.representation;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Dell
 *
 */
@XmlRootElement
public class RestResponseMajDTO {
	
	private String resultatOpertion;

	/**
	 * @return the resultatOpertion
	 */
	public String getResultatOpertion() {
		return resultatOpertion;
	}

	/**
	 * @param pResultatOpertion the resultatOpertion to set
	 */
	public void setResultatOpertion(String pResultatOpertion) {
		resultatOpertion = pResultatOpertion;
	}
	
	

}
