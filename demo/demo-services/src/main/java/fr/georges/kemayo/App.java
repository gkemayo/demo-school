package fr.georges.kemayo;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import fr.georges.kemayo.wsrest.resource.EtudiantWebServiceRestRessource;

/**
 * Hello world!
 *
 */
public class App {
	
	public static final URI BASE_URI = getBaseURI();

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost").port(9991).build();
	}

	public static void main(String[] args) {
		ResourceConfig rc = new ResourceConfig();
		rc.packages("fr.georges.kemayo");
		rc.registerClasses(EtudiantWebServiceRestRessource.class);
		try {
			HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, rc);
			server.start();

			System.out.println(String.format(
					"Jersey app started with WADL available at " + "%sapplication.wadl\nHit enter to stop it...",
					BASE_URI, BASE_URI));
			System.in.read();
			server.shutdownNow();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
