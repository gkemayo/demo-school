/**
 * 
 */
package fr.georges.kemayo.mapper;

import org.modelmapper.ModelMapper;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.dtoapi.EtudiantDTO;

/**
 * @author Dell
 *
 */
public class EtudiantServiceMapper {
	
	/**
	 * 
	 */
	private EtudiantServiceMapper() {
		//empty constructor
	}

	public static EtudiantDto mapEtudiantDTOtoEtudiantDto(EtudiantDTO etu) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(etu, EtudiantDto.class);
	}
	
	public static EtudiantDTO getEtudiantDTOFromEtudiantDto(EtudiantDto pEtudiantDto) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(pEtudiantDto, EtudiantDTO.class);
	}

}
