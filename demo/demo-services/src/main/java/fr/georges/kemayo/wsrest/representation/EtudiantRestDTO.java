/**
 * 
 */
package fr.georges.kemayo.wsrest.representation;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Dell
 *
 */
//@XmlRootElement(name = "Etudiant")
@JsonInclude(content = Include.NON_EMPTY)
public class EtudiantRestDTO {
	
	public EtudiantRestDTO(){
		
	}
	
	/**
	 * @param pAge
	 * @param pNom
	 * @param pPrenom
	 */
	public EtudiantRestDTO(Integer pAge, String pNom, String pPrenom) {
		super();
		age = pAge;
		nom = pNom;
		prenom = pPrenom;
	}



	private Integer matricule;

	private Integer age;

	private String nom;

	private String prenom;

	public Integer getMatricule() {
		return matricule;
	}

	public void setMatricule(Integer pMatricule) {
		matricule = pMatricule;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer pAge) {
		age = pAge;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String pNom) {
		nom = pNom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String pPrenom) {
		prenom = pPrenom;
	}

}
