package fr.georges.kemayo.managerpresentation;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.dtoapi.EtudiantDTO;
import fr.georges.kemayo.imanager.EtudiantManager;
import fr.georges.kemayo.imanagerpresentation.EtudiantManagerPresentation;
import fr.georges.kemayo.mapper.EtudiantServiceMapper;

/**
 * @author Dell
 *
 */
@Service("etudiantManagerPresentation")
public class EtudiantManagerPresentationImpl implements EtudiantManagerPresentation {
	
	private static final Logger LOG = LogManager.getLogger(EtudiantManagerPresentationImpl.class);
	
	@Autowired
	private EtudiantManager etudiantManager;
	
	public EtudiantDTO creerEtudiant(EtudiantDTO pEtudiantDTO){
		EtudiantDto etudiantDto = EtudiantServiceMapper.mapEtudiantDTOtoEtudiantDto(pEtudiantDTO);
		etudiantDto = etudiantManager.creerEtudiant(etudiantDto);
		return EtudiantServiceMapper.getEtudiantDTOFromEtudiantDto(etudiantDto);
		
	}

}
