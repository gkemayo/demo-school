/**
 * 
 */
package fr.georges.kemayo.configspring_java;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Dell
 *
 */
@Configuration
@ComponentScan("fr.georges.kemayo.manager")
public class ApplicationMetierSpringContext {

}
