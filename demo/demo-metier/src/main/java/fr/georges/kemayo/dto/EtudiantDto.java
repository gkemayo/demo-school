/**
 * 
 */
package fr.georges.kemayo.dto;

import java.io.Serializable;

/**
 * @author Dell
 *
 */
public class EtudiantDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7708568322723240959L;
	
	/**
	 * 
	 */
	public EtudiantDto() {
		//constructeur vide
	}

	private Integer matricule;
	
	private Integer age;
	
	private String nom;
	
	private String prenom;

	public Integer getMatricule() {
		return matricule;
	}

	public void setMatricule(Integer pMatricule) {
		matricule = pMatricule;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer pAge) {
		age = pAge;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String pNom) {
		nom = pNom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String pPrenom) {
		prenom = pPrenom;
	}
	
}
