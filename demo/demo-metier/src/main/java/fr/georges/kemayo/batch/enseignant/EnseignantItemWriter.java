/**
 * 
 */
package fr.georges.kemayo.batch.enseignant;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import fr.georges.kemayo.idao.EnseignantDao;
import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
public class EnseignantItemWriter implements ItemWriter<Enseignant> {
	
	@Autowired
	private EnseignantDao enseignantDao;
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	public void write(List<? extends Enseignant> pItems) throws Exception {
		for (Enseignant enseignant : pItems) {
			enseignantDao.saveEnseignant(enseignant);
		}
	}
	
	

}
