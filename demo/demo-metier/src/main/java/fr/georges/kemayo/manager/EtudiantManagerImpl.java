/**
 * 
 */
package fr.georges.kemayo.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.idao.EtudiantDao;
import fr.georges.kemayo.imanager.EtudiantManager;
import fr.georges.kemayo.mapper.EtudiantMetierMapper;
import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
@Service("etudiantManager")
@Transactional
public class EtudiantManagerImpl implements EtudiantManager {
	
	@Autowired
	private EtudiantDao etudiantDao;

	public EtudiantDto creerEtudiant(EtudiantDto pEtudiantDto) {
		
		Etudiant etudiant = EtudiantMetierMapper.getEtudiantFromEtudiantDto(pEtudiantDto);
		Integer id = etudiantDao.saveEtudiant(etudiant);
		pEtudiantDto.setMatricule(id);
		return pEtudiantDto;
	}

	/* (non-Javadoc)
	 * @see fr.georges.kemayo.imanager.EtudiantManager#getAllEtudiants()
	 */
	@Override
	public List<EtudiantDto> getAllEtudiants() {
		List<Etudiant> listEtu = etudiantDao.getAllEtudiants();
		List<EtudiantDto> listEtuDto = new ArrayList<>();
		for (Etudiant etudiant : listEtu) {
			listEtuDto.add(EtudiantMetierMapper.getEtudiantDtoFromEtudiant(etudiant));
		}
		return listEtuDto;
	}

	/* (non-Javadoc)
	 * @see fr.georges.kemayo.imanager.EtudiantManager#getOneEtudiants(java.lang.Integer)
	 */
	@Override
	public EtudiantDto getEtudiantByMatricule(Integer pMatriculeEtu) {
		Etudiant etu = etudiantDao.getEtudiantByMatricule(pMatriculeEtu);
		return EtudiantMetierMapper.getEtudiantDtoFromEtudiant(etu);
	}

	/* (non-Javadoc)
	 * @see fr.georges.kemayo.imanager.EtudiantManager#deleteEtudiantByMatricule(java.lang.Integer)
	 */
	@Override
	public Integer deleteEtudiantByNom(String pNomEtu) {
		return etudiantDao.deleteEtudiantByNom(pNomEtu);
	}
	
}
