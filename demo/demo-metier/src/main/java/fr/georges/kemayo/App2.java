package fr.georges.kemayo;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App2 {
		
    public static void main( String[] args ) throws Exception {
    	
    	try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:ApplicationSpringBatchContext.xml");) {
			SimpleJobLauncher jobLaucher = (SimpleJobLauncher) context.getBean("jobLauncherWithoutSpringBatchMetadataDataBase");
			Job job = (Job) context.getBean("jobChargementEnseignants");
			
			JobParameters paramaters = new JobParametersBuilder().addLong("temps", System.currentTimeMillis()).addString("filePath", "E:\\Dev\\workspace\\demo-data\\enseignantsBatchDataFile.csv").toJobParameters();
			jobLaucher.run(job, paramaters);
		} 
    }
}
