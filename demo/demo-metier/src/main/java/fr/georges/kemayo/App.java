package fr.georges.kemayo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.georges.kemayo.batch.enseignant.EnseignantJobRunner;
import fr.georges.kemayo.configspring_java.ApplicationSpringBatchContext;

/**
 * Hello world!
 *
 */
public class App {
		
    public static void main( String[] args ) {
    	
    	AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationSpringBatchContext.class);
    	EnseignantJobRunner enseignantJobLauncher = (EnseignantJobRunner) context.getBean("enseignantJobLauncher");
    	enseignantJobLauncher.run();
    	
    	context.close();
    }
}
