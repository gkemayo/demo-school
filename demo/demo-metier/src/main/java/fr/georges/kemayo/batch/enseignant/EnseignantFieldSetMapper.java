/**
 * 
 */
package fr.georges.kemayo.batch.enseignant;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
public class EnseignantFieldSetMapper implements FieldSetMapper<Enseignant> {
	
	/**
	 * 
	 */
	public EnseignantFieldSetMapper() {
		// default constructor
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	public Enseignant mapFieldSet(FieldSet pFielSet) throws BindException {
		Enseignant ens = new Enseignant();
		ens.setNom(pFielSet.readString("nom"));
		ens.setPrenom(pFielSet.readString("prenom"));
		ens.setAge(pFielSet.readInt("age"));
		return ens;
	}
	
	

}
