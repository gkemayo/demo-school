/**
 * 
 */
package fr.georges.kemayo.batch.enseignant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
@Component
public class JobEnseignantNotificationListener extends JobExecutionListenerSupport {
	
	private static final Logger LOG = LogManager.getLogger(JobEnseignantNotificationListener.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * 
	 */
	public JobEnseignantNotificationListener() {
		// default constructor
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.listener.JobExecutionListenerSupport#afterJob(org.springframework.batch.core.JobExecution)
	 */
	@Override
	public void afterJob(JobExecution pJobExecution) {
		if(pJobExecution.getStatus() == BatchStatus.COMPLETED) {
			LOG.info("!!! JOB Terminé! Vérification des résultats...");

			List<Enseignant> results = jdbcTemplate.query("SELECT IDENS, NOM, PRENOM, AGE FROM DEMODB.DEMO_ENSEIGNANT", new RowMapper<Enseignant>() {
				/* (non-Javadoc)
				 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
				 */
				public Enseignant mapRow(ResultSet pRs, int pRowNum) throws SQLException { 
					Enseignant ens = new Enseignant(pRs.getString(2), pRs.getString(3), pRs.getInt(4));
					ens.setIdEns(pRs.getInt(1));
					return ens;
				}
				
			});

			for (Enseignant enseignant : results) {
				System.out.println("<" + enseignant + "> Trouvé en base de données.");
				LOG.info("<" + enseignant + "> Trouvé en base de données.");
			}

		}
	}
	
	
	
	

}
