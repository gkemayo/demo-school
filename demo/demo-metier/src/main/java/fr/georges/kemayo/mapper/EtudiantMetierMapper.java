/**
 * 
 */
package fr.georges.kemayo.mapper;

import org.modelmapper.ModelMapper;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
public class EtudiantMetierMapper {

	/**
	 * 
	 */
	private EtudiantMetierMapper() {
		//empty constructor
	}

	public static Etudiant getEtudiantFromEtudiantDto(EtudiantDto etu) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(etu, Etudiant.class);
	}
	
	public static EtudiantDto getEtudiantDtoFromEtudiant(Etudiant etu) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(etu, EtudiantDto.class);
	}


}
