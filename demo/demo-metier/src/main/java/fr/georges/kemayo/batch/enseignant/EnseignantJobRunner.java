/**
 * 
 */
package fr.georges.kemayo.batch.enseignant;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author Dell
 *
 */
@Component("enseignantJobLauncher")
public class EnseignantJobRunner {
	
	@Autowired
	@Qualifier("jobChargementEnseignants")
	private Job jobChargementEnseignants;
	
	@Autowired
	@Qualifier("jobLauncherWithoutSpringBatchMetadataDataBase")
	private JobLauncher jobLauncherWithoutSpringBatchMetadataDataBase;
	
//	@Autowired
//	@Qualifier("jobLauncherWithSpringBatchDataBase")
//	private JobLauncher jobLauncherWithSpringBatchDataBase;
	
	public void run(){
		JobParameters paramaters = new JobParametersBuilder().addLong("temps", System.currentTimeMillis()).addString("filePath", "E:\\Dev\\workspace\\demo-data\\enseignantsBatchDataFile.csv").toJobParameters();
		try {
			jobLauncherWithoutSpringBatchMetadataDataBase.run(jobChargementEnseignants, paramaters);
		} catch (JobExecutionAlreadyRunningException e) {
	    } catch (JobRestartException e) {
	    } catch (JobInstanceAlreadyCompleteException e) {
	    } catch (JobParametersInvalidException e) {
	    }
	}

}
