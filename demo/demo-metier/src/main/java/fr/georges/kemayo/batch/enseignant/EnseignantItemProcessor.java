/**
 * 
 */
package fr.georges.kemayo.batch.enseignant;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
public class EnseignantItemProcessor implements ItemProcessor<Enseignant, Enseignant>{
	
	private static Logger LOG = LogManager.getLogger(EnseignantItemProcessor.class);

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public Enseignant process(final Enseignant pEnseignant) throws Exception {
		final String nom = pEnseignant.getNom().toUpperCase();
		String c = pEnseignant.getPrenom().substring(0, 1);
		c = c.toUpperCase();
		final String prenom = new StringBuilder().append(c).append(pEnseignant.getPrenom().substring(1)).toString();
		final Integer age = pEnseignant.getAge();
		
		Enseignant enseignantTraite = new Enseignant(nom, prenom, age);
		LOG.info("Enseignant issu du fichier " + pEnseignant + "Traitement de l'enseignant " + enseignantTraite);
		
		return enseignantTraite;
	}
	
	

}
