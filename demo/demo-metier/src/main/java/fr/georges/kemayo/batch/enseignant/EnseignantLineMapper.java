/**
 * 
 */
package fr.georges.kemayo.batch.enseignant;

import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;

import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
public class EnseignantLineMapper implements LineMapper<Enseignant> {
	
	private DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer(";");
	
	private EnseignantFieldSetMapper fieldSetMapper = new EnseignantFieldSetMapper();
	
	/**
	 * 
	 */
	public EnseignantLineMapper() {
		//default constructor
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.file.LineMapper#mapLine(java.lang.String, int)
	 */
	public Enseignant mapLine(String pLine, int pLineNumber) throws Exception {
		lineTokenizer.setNames(new String[]{"nom", "prenom", "age"});
		return fieldSetMapper.mapFieldSet(lineTokenizer.tokenize(pLine));
	}

}
