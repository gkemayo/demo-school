/**
 * 
 */
package fr.georges.kemayo.configspring_java;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import fr.georges.kemayo.batch.JobParameterBatch;
import fr.georges.kemayo.batch.enseignant.EnseignantItemProcessor;
import fr.georges.kemayo.batch.enseignant.EnseignantItemWriter;
import fr.georges.kemayo.batch.enseignant.EnseignantLineMapper;
import fr.georges.kemayo.batch.enseignant.JobEnseignantNotificationListener;
import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
@Configuration
@ComponentScan(basePackages = { "fr.georges.kemayo.configspring_java", "fr.georges.kemayo.batch" })
@EnableBatchProcessing
public class ApplicationSpringBatchContext {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private DataSource getDataSource;

	@Autowired
	PlatformTransactionManager workTransactionManager;
	
	@Autowired
	HibernateTransactionManager getHibernateTransactionManager;

	@Autowired
	private JobParameterBatch jobParameterBatch;
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(getDataSource);
	}

	// --- Début création Beans permettant au batch de ne pas utiliser de base
	// de données pour ses métadonnées statistiques (car par défaut spring batch
	// utilise une
	// BD pour réaliser des stats sur le batch)
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}

	@Bean
	@Qualifier("jobRepositoryWithoutSpringBatchMetadataDataBase")
	public JobRepository jobRepository() throws Exception {
		//le nom de cette méthode jobRepository() ne doit pas changé, sinon ça ne marche pas
		return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
	}

	@Bean
	@Qualifier("jobLauncherWithoutSpringBatchMetadataDataBase")
	public JobLauncher jobLauncherWithoutSpringBatchMetadataDataBase() throws Exception {
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setJobRepository(jobRepository());
		return jobLauncher;
	}
	// --- Fin création Beans permettant au batch de ne pas utiliser de
	// base de données pour ses statistiques

	// --- Début création Beans permettant au batch d'utiliser une base de
	// données pour ses statistiques
	@Bean
	@Qualifier("jobRepositoryWithSpringBatchMetadataDataBase")
	public JobRepository jobRepositoryWithSpringBatchMetadataDataBase() throws Exception {
		JobRepositoryFactoryBean jobRepoFactory = new JobRepositoryFactoryBean();
		jobRepoFactory.setDatabaseType("Oracle");
		jobRepoFactory.setTransactionManager(workTransactionManager);
		jobRepoFactory.setDataSource(getDataSource);
		jobRepoFactory.setTablePrefix("DEMOD.BATCH_");
		return jobRepoFactory.getObject();
	}

	@Bean
	@Qualifier("jobLauncherWithSpringBatchMetadataDataBase")
	public JobLauncher jobLauncherWithSpringBatchMetadataDataBase() throws Exception {
		SimpleJobLauncher jobL = new SimpleJobLauncher();
		jobL.setJobRepository(jobRepositoryWithSpringBatchMetadataDataBase());
		return jobL;
	}
	// --- Fin création Beans permettant au batch d'utiliser une base de données
	// pour ses statistiques

	/*--- Beans pour le batch de chargement des enseignants ---*/
	@Bean
	@StepScope
	public FlatFileItemReader<Enseignant> enseignantItemReader() {

		FlatFileItemReader<Enseignant> donneeDentree = new FlatFileItemReader<Enseignant>();
		donneeDentree.setResource(new FileSystemResource(jobParameterBatch.getFilePath()));
		donneeDentree.setLineMapper(new EnseignantLineMapper());
		return donneeDentree;

	}

	@Bean
	public EnseignantItemProcessor enseignantItemProcessor() {
		return new EnseignantItemProcessor();
	}

	@Bean
	public EnseignantItemWriter enseignantItemWriter() {
		return new EnseignantItemWriter();
	}

	@Bean
	public Step stepChargementEnseignants() {
		return stepBuilderFactory.get("stepChargementEnseignants").transactionManager(workTransactionManager)
				.<Enseignant, Enseignant>chunk(3).reader(enseignantItemReader()).processor(enseignantItemProcessor())
				.writer(enseignantItemWriter()).build();
	}

	@Bean
	@Qualifier("jobChargementEnseignants")
	public Job jobChargementEnseignants(JobEnseignantNotificationListener listener) {
		return jobBuilderFactory.get("jobChargementEnseignants").incrementer(new RunIdIncrementer()).listener(listener)
				.flow(stepChargementEnseignants()).end().build();
	}
	/*--- Fin Beans pour le batch de chargement des enseignants ---*/

}
