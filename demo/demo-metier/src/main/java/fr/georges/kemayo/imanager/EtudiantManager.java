/**
 * 
 */
package fr.georges.kemayo.imanager;

import java.util.List;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
public interface EtudiantManager {

	public EtudiantDto creerEtudiant(EtudiantDto pEtudiantDto);

	public List<EtudiantDto> getAllEtudiants();

	public EtudiantDto getEtudiantByMatricule(Integer matriculeEtu);
	
	public Integer deleteEtudiantByNom(String pNomEtu);

}
