/**
 * 
 */
package fr.georges.kemayo.batch;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Dell
 *
 */
@Component
@StepScope
public class JobParameterBatch {
	
	@Value("#{jobParameters['filePath']}")
	private String filePath;

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

}
