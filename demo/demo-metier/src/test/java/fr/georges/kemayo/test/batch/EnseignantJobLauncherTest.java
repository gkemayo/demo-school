/**
 * 
 */
package fr.georges.kemayo.test.batch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import fr.georges.kemayo.config.TestBatchWithBaseEmbarqueeConfigInitializer;
import fr.georges.kemayo.model.Enseignant;

/**
 * @author Dell
 *
 */
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class EnseignantJobLauncherTest extends TestBatchWithBaseEmbarqueeConfigInitializer {
	
	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;
	
	@Autowired
	@Qualifier("jobRepositoryWithoutSpringBatchMetadataDataBase")
	private JobRepository jobRepository;
	
	@Autowired
	@Qualifier("jobChargementEnseignants")
	private Job job;
	
	@Autowired
	@Qualifier("jobLauncherWithoutSpringBatchMetadataDataBase")
	private JobLauncher jobLauncherWithoutSpringBatchMetadataDataBase;
	
	private File temporalFile;
	
	private File databaseRepresentationResultFile;
	
	@Before
	public void setUp() throws Exception {
		temporalFile = File.createTempFile("enseignantsBatchDataFile", ".csv");
		try (PrintWriter printer = new PrintWriter(new FileWriter(temporalFile))){
			//bloc try with resources
			printer.println("Kemayo;Georges;30");
			printer.println("Meseure;philippe;40");
			printer.println("Merieux;michel;37");
		} 
		//		BufferedWriter bw = new BufferedWriter(new FileWriter(temporalFile));
		//		bw.write("Kemayo;Georges;30"); 
		//		bw.newLine();
		//		bw.write("Meseure;philippe;40");
		//		bw.newLine();
		//		bw.write("Merieux;michel;37");
		//		bw.close();
		
		databaseRepresentationResultFile = File.createTempFile("result", ".csv");
		try (PrintWriter printerRes = new PrintWriter(new FileWriter(databaseRepresentationResultFile))){
			printerRes.println("KEMAYO;Georges;30");
			printerRes.println("MESEURE;Philippe;40");
			printerRes.println("MERIEUX;Michel;37");
		} 
		
	}
	
	@Test
	public void testJobChargementEnseignant() throws Exception {
		
		jobLauncherTestUtils.setJobLauncher(jobLauncherWithoutSpringBatchMetadataDataBase);
		JobParameters paramaters = new JobParametersBuilder().addString("filePath", temporalFile.getAbsolutePath()).toJobParameters();
		jobLauncherTestUtils.setJob(job);

		JobExecution jobExecution = jobLauncherTestUtils.launchJob(paramaters);
		
		//JobExecution jobExecution = jobLauncherTestUtils.launchStep("step1");
		
		assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
		
		List<Enseignant> listResult = hibernateTemplate.loadAll(Enseignant.class);
		assertNotNull(listResult);
		
		BufferedReader br = new BufferedReader(new FileReader(databaseRepresentationResultFile));
		for (Enseignant enseignant : listResult) {
			StringBuilder sb = new StringBuilder();
			sb.append(enseignant.getNom()).append(";").append(enseignant.getPrenom()).append(";").append(enseignant.getAge());
			if(enseignant != null){
				String ligne = br.readLine();
				assertEquals(ligne, sb.toString());
			}
		}
		br.close();
	}
	
	@After
	public void tearDown(){
		temporalFile.delete();
		databaseRepresentationResultFile.delete();
	}
	
}
