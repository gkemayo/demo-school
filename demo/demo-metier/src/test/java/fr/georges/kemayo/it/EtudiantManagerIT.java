/**
 * 
 */
package fr.georges.kemayo.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fr.georges.kemayo.config.TestIntegrationMetierWithBaseOracleConfigInitializer;
import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.imanager.EtudiantManager;

/**
 * @author Dell
 *
 */
public class EtudiantManagerIT extends TestIntegrationMetierWithBaseOracleConfigInitializer {
	
	@Autowired
	private EtudiantManager etudiantManager;
	
	private EtudiantDto etudiantDto;
	
	@Before
	public void setUp(){
		etudiantDto = new EtudiantDto();
		etudiantDto.setAge(30);
		etudiantDto.setNom("Kemayo Test");
		etudiantDto.setPrenom("Georges Test");
	}
	
	@Test
	public void testCreerEtudiant(){
		EtudiantDto etudiantDtoResult = etudiantManager.creerEtudiant(etudiantDto);
		
		assertNotNull(etudiantDtoResult);
		assertNotNull(etudiantDtoResult.getMatricule());
		assertEquals(etudiantDto.getAge(), etudiantDtoResult.getAge());
		assertEquals(etudiantDto.getNom(), etudiantDtoResult.getNom());
		assertEquals(etudiantDto.getPrenom(), etudiantDtoResult.getPrenom());
		
	}

}
