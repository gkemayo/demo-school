/**
 * 
 */
package fr.georges.kemayo.test.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.mapper.EtudiantMetierMapper;
import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class EtudiantMetierMapperTest {
	
	private EtudiantDto etudiantDto;
	
	@Before
	public void setUp(){
		etudiantDto = new EtudiantDto();
		etudiantDto.setAge(30);
		etudiantDto.setNom("Kemayo Test");
		etudiantDto.setPrenom("Georges Test");
	}
	
	@Test
	public void testGetEtudiantFromEtudiantDto(){
		
		Etudiant etudiant = EtudiantMetierMapper.getEtudiantFromEtudiantDto(etudiantDto);
		
		assertNotNull(etudiant);
		assertEquals(etudiantDto.getAge(), etudiant.getAge());
		assertEquals(etudiantDto.getNom(), etudiant.getNom());
		assertEquals(etudiantDto.getPrenom(), etudiant.getPrenom());
		
	}

}
