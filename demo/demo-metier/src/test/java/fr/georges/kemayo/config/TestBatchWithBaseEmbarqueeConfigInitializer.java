/**
 * 
 */
package fr.georges.kemayo.config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import fr.georges.kemayo.configspring_java.ApplicationDaoSpringContext;
import fr.georges.kemayo.configspring_java.ApplicationMetierSpringContext;
import fr.georges.kemayo.configspring_java.ApplicationSpringBatchContext;

/**
 * @author Dell
 *
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@TestExecutionListeners(DependencyInjectionTestExecutionListener.class)
@ContextConfiguration(classes = {SpringBatchConfigTest.class, ApplicationSpringBatchContext.class, ApplicationMetierSpringContext.class, ApplicationDaoSpringContext.class, HibernateTestConfigForBatch.class})
//le fait de mettre HibernateTestConfigForBatch.class en dernier, permet de surcharger la datasource 'getDataSource' définie dans 
//ApplicationSpringBatchContext par celle de HibernateTestConfigForBatch qui injecte les paramètres de la bse embarquée hsqldb 
public abstract class TestBatchWithBaseEmbarqueeConfigInitializer extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	protected HibernateTemplate hibernateTemplate;

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSessionForTest() {
		return sessionFactory.getCurrentSession();
	}

}
