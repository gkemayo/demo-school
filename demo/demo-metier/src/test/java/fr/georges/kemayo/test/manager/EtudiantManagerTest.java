/**
 * 
 */
package fr.georges.kemayo.test.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.annotation.DirtiesContext;

import fr.georges.kemayo.dto.EtudiantDto;
import fr.georges.kemayo.idao.EtudiantDao;
import fr.georges.kemayo.imanager.EtudiantManager;
import fr.georges.kemayo.manager.EtudiantManagerImpl;
import fr.georges.kemayo.model.Etudiant;

/**
 * @author Dell
 *
 */
public class EtudiantManagerTest {

	@InjectMocks
	private EtudiantManager etudiantManager = new EtudiantManagerImpl();
	
	@Mock
	private EtudiantDao etudiantDao;

	private EtudiantDto etudiantDto;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		//etudiantDao = Mockito.mock(EtudiantDao.class);

		etudiantDto = new EtudiantDto();
		etudiantDto.setAge(30);
		etudiantDto.setNom("Kemayo Test");
		etudiantDto.setPrenom("Georges Test");
	}

	@Test
	@DirtiesContext
	public void testCreerEtudiant() {
		
		Mockito.when(etudiantDao.saveEtudiant(Mockito.any(Etudiant.class))).thenReturn(1);

		EtudiantDto etudiantDtoResult = etudiantManager.creerEtudiant(etudiantDto);
		
		Mockito.verify(etudiantDao).saveEtudiant(Mockito.any(Etudiant.class));

		assertNotNull(etudiantDtoResult);
		assertNotNull(etudiantDtoResult.getMatricule());
		assertEquals(etudiantDto.getAge(), etudiantDtoResult.getAge());
		assertEquals(etudiantDto.getNom(), etudiantDtoResult.getNom());
		assertEquals(etudiantDto.getPrenom(), etudiantDtoResult.getPrenom());

	}

}
